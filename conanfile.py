from conans import ConanFile, CMake, tools
import shutil

from conans.errors import ConanInvalidConfiguration

class QuillConan(ConanFile):
    name            = "quill"
    version         = "1.5.2"
    license         = "MIT"
    author          = "toge.mail@gmail.com"
    url             = "https://bitbucket.org/toge/conan-quill"
    homepage        = "https://github.com/odygrd/quill"
    description     = "Asynchronous Low Latency Logging Library"
    topics          = ("logging", "asynchronous")
    settings        = "os", "compiler", "build_type", "arch"
    generators      = "cmake", "cmake_find_package"
    options         = {"with_no_exceptions": [True, False], "with_bounded_queue": [True, False]}
    default_options = {"with_no_exceptions": False, "with_bounded_queue": False}

    @property
    def _supported_cppstd(self):
        return ["14", "gnu14", "17", "gnu17", "20", "gnu20"]

    def _has_support_for_cpp14(self):
        supported_compilers = [('gcc', 5), {"clang", 5}, {"app-clang", 10}, {"Visual Studio", 14.3}]
        compiler, version = self.settings.compiler, tools.Version(self.settings.compiler.version)
        return any(compiler == sc[0] and version >= sc[1] for sc in supported_compilers)

    def configure(self):
        if self.settings.compiler.cppstd and not self.settings.compiler.cppstd in self._supported_cppstd:
            raise ConanInvalidConfiguration("This library requires c++14 or higher. {} required.".format(self.settings.compiler.cppstd))
        if not self._has_support_for_cpp14():
            raise ConanInvalidConfiguration("This library requires c++14 or higher support standard. {} {} is not supported.".format(self.settings.compiler, self.settings.compiler.version))

    def source(self):
        tools.get("https://github.com/odygrd/quill/archive/v{}.zip".format(self.version))
        shutil.move("quill-{}".format(self.version), "quill")

        tools.replace_in_file("quill/CMakeLists.txt", "project(my_project)",
                              '''project(my_project)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def requirements(self):
        # if self.options.fmt_external:
        self.requires("fmt/[>= 7.1.2]")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["QUILL_SANITIZE_ADDRESS"] = self.options.sanitize_address
        cmake.definitions["QUILL_SANITIZE_THREAD"]  = self.options.sanitize_thread
        cmake.definitions["QUILL_FMT_EXTERNAL"] = True
        cmake.definitions["QUILL_NO_EXCEPTIONS"] = self.options.with_no_exceptions
        cmake.definitions["QUILL_USE_BOUNDED_QUEUE"] = self.options.with_bounded_queue
        cmake.configure(source_folder="quill")
        cmake.build()

    def package(self):
        self.copy("*.h",     dst="include/quill", src="quill/quill/include/quill")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.defines = ["QUILL_FMT_EXTERNAL"]
        self.cpp_info.libs = ["quill"]
        if self.settings.os == "Linux": 
            self.cpp_info.libs.append("pthread")
