#include <iostream>
#include "quill/Quill.h"

int main() {
    quill::start();

    auto logger = quill::get_logger();

    return 0;
}
